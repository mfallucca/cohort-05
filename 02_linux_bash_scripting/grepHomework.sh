for i in $(cat ccNumbers)
do
    RED='\033[0;31m'
    BLUE='\033[0;34m'
    GREEN='\033[0;32m'
    YELLOW='\033[1;33m'
    MAGENTA='\033[0;35m'
    GRAY='\033[1;30m'
    NC='\033[0m' # No Color
    if (grep -qP '([0-9]{4}-){3}[0-9]{4}' <<< $i) && (grep -qP '^[4]' <<< $i)
    then
        echo -e "$i is a ${BLUE}Visa Card${NC}"
    elif (grep -qP '([0-9]{4}-){3}[0-9]{4}' <<< $i) && (grep -qP '^[5]' <<< $i)
    then
        echo -e "$i is a ${RED}Master${YELLOW}Card${NC}"
    elif (grep -qP '([0-9]{4}-){3}[0-9]{4}' <<< $i) && (grep -qP '^[6]' <<< $i)
    then
        echo -e "$i is a ${MAGENTA}Discover Card${NC}"
    elif (grep -qP '([0-9]{4}-){3}[0-9]{3}' <<< $i) && (grep -qP '^[3][47]' <<< $i)
    then
        echo -e "$i is an ${GREEN}American Express Card${NC}"
    elif (grep -qP '([0-9]{4}-){3}[0-9]{2}' <<< $i) && (grep -qP '^[3][068]' <<< $i)
    then
        echo -e "$i is a ${GRAY}Diner's Club Card${NC}"
    else
        echo -e "$i \e[5mdoes not match any known credit card brand pattern\e[25m"
    fi
done


    # Visa cards – Begin with a 4 and have 13 or 16 digits
    # Mastercard cards – Begin with a 5 and has 16 digits
    # American Express cards – Begin with a 3, followed by a 4 or a 7  has 15 digits
    # Discover cards – Begin with a 6 and have 16 digits
    # Diners Club and Carte Blanche cards – Begin with a 3, followed by a 0, 6, or 8 and have 14 digits
